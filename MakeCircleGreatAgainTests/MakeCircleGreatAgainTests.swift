//
//  MakeCircleGreatAgainTests.swift
//  MakeCircleGreatAgainTests
//
//  Created by Andrea Ruffino on 06/03/2019.
//  Copyright © 2019 Andrea Ruffino. All rights reserved.
//

import XCTest
import Mapbox
@testable import MakeCircleGreatAgain

class MakeCircleGreatAgainTests: XCTestCase {

    var route: RouteAnnotation!
    let randomBoundRect = CGRect(x: 50, y: 50, width: 5, height: 5)
    
    override func setUp() {
        route = RouteAnnotation(waypoints: [])
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        route = nil
    }

    func testIntegrity() {
//        XCTAssertEqual(route.waypointCount, route.coordinateCountPerWaypoint.count)
//
//        for (wIndex, waypoint) in route.waypointAnnotation.enumerated() {
//            let cIndex = Int(route.coordinateIndexForWaypoint(at: wIndex))
//            let fromWaypoint = waypoint.coordinate
//            let fromCoordinate = route.coordinates[cIndex]
//            XCTAssertEqual(fromWaypoint, fromCoordinate)
//        }
    }
    
    func testInsertion() {
//        // First insertion
//        var randomCoordinate = CLLocationCoordinate2D(
//            latitude:  Double.random(in: Double(randomBoundRect.minY) ... Double(randomBoundRect.maxY)),
//            longitude: Double.random(in: Double(randomBoundRect.minX) ... Double(randomBoundRect.maxX)))
//
//        route.insert(coordinate: randomCoordinate, at: route.waypointCount)
//
//        testIntegrity()
//        XCTAssertEqual(route.waypointCount, 1)
//        XCTAssertGreaterThanOrEqual(route.pointCount, 1)
//
//        // Second insertion
//        randomCoordinate = CLLocationCoordinate2D(
//            latitude:  Double.random(in: Double(randomBoundRect.minY) ... Double(randomBoundRect.maxY)),
//            longitude: Double.random(in: Double(randomBoundRect.minX) ... Double(randomBoundRect.maxX)))
//
//        route.insert(coordinate: randomCoordinate, at: route.waypointCount)
//
//        testIntegrity()
//        XCTAssertEqual(route.waypointCount, 2)
//        XCTAssertGreaterThanOrEqual(route.pointCount, 2)
//
//        // Third insertion
//        randomCoordinate = CLLocationCoordinate2D(
//            latitude:  Double.random(in: Double(randomBoundRect.minY) ... Double(randomBoundRect.maxY)),
//            longitude: Double.random(in: Double(randomBoundRect.minX) ... Double(randomBoundRect.maxX)))
//
//        route.insert(coordinate: randomCoordinate, at: route.waypointCount)
//
//        testIntegrity()
//        XCTAssertEqual(route.waypointCount, 3)
//        XCTAssertGreaterThanOrEqual(route.pointCount, 3)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
