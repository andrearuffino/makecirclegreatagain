## Privacy Policy

Andrea Ruffino built the MakeCircleGreatAgain app as a Free app. This SERVICE is provided by Andrea Ruffino at no cost and is intended for use as is.

This page is used to inform visitors regarding my policies with the collection, use, and disclosure of Personal Information if anyone decided to use my Service.

**Information Collection and Use**

The app does use a third party Framework which is related to geolocation.

Link to privacy policy of third party service providers used by the app:

*   [**Mapbox**](https://www.mapbox.com/privacy/)

From Mapbox privacy policy: "*If you are an end user of a product or service that integrates our Services, your privacy options will be largely determined by the developer of the product or service. In addition to any privacy options that the developer may have provided you with, you may also be able to control the applications that can collect information about your precise location by using the settings available on your device.*"

Nevertheless, no identifiable geolocation point nor region is retrieved and no personal data is used nor stored from/into the MakeCircleGreatAgain app.

**Service Providers**

I may employ third-party frameworks due to the following reasons:

*   To provide the Service on our behalf;
*   To perform geolocation-related services;

**Links to Other Sites**

This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by me. Therefore, I strongly advise you to review the Privacy Policy of these websites. I have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.

**Changes to This Privacy Policy**

I may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. I will notify you of any changes by posting the new Privacy Policy on this page. These changes are effective immediately after they are posted on this page.

**Contact Us**

If you have any questions or suggestions about my Privacy Policy, do not hesitate to contact [**us**](mailto:andrea.ruffino@hotmail.fr).
