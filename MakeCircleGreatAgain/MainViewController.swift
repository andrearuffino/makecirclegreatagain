//
//  ViewController.swift
//  MakeCircleGreatAgain
//
//  Created by Andrea Ruffino on 08/12/2018.
//  Copyright © 2018 Andrea Ruffino. All rights reserved.
//

import UIKit
import CoreLocation
import ImageProc

class MainViewController: UIViewController {
    
    var mapViewController: MapViewController!
    
    @IBOutlet weak var mapContainerView: UIView!
    
    @IBOutlet weak var toolBoxContainerView: UIView!
    
    @IBOutlet weak var toolTipContainerView: UIView!
    
    @IBOutlet weak var toolBoxWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var toolTipHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var helpButton: UIButton!
    
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBOutlet weak var distanceLabel: UILabel!
    
    private var isToolBoxHidden: Bool = true
    
    private var isToolTipHidden: Bool = true
    
    private let toolBoxMaxWidth: CGFloat = 200
    
    private let toolBoxMinWidth: CGFloat = 84
    
    private let toolTipMaxHeight: CGFloat = 80
    
    private let toolTipMinHeight: CGFloat = 0
    
    private var isEditModeOn: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editButton.setImage(UIImage(named: "pencil-icon")!.colorized(with: UIColor.white), for: .selected)
        helpButton.setImage(UIImage(named: "tooltip-icon")!.colorized(with: UIColor.white), for: .selected)
        progressView.isHidden = true
        toolTipContainerView.isHidden = true
        toolTipHeightConstraint.constant = toolTipMinHeight
        toolBoxWidthConstraint.constant = toolBoxMinWidth
        mapViewController.singleTapGesture.isEnabled = isEditModeOn
        mapViewController.longPressGesture.isEnabled = isEditModeOn
        initContainerView(toolBoxContainerView)
        initContainerView(toolTipContainerView)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MainToMap" {
            mapViewController = segue.destination as? MapViewController
            mapViewController.delegate = self
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func initContainerView(_ containerView: UIView) {
        containerView.backgroundColor = UIColor.clear
        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = 10
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = UIColor.white.withAlphaComponent(0.6).cgColor
        
        let blurEffect = UIBlurEffect(style: .light)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        containerView.insertSubview(blurView, at: 0)
        
        NSLayoutConstraint.activate([
            blurView.heightAnchor.constraint(equalTo: containerView.heightAnchor),
            blurView.widthAnchor.constraint(equalTo: containerView.widthAnchor),
            blurView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            blurView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor)
            ])
    }
    
    @IBAction func onToolTipButtonTouchUpInside(_ sender: UIButton) {
        if isToolTipHidden {
            showToolTip()
        } else {
            hideToolTip()
        }
    }
    
    @IBAction func onPencilButtonTouchUpInside(_ sender: UIButton) {
        isEditModeOn.toggle()
        sender.isSelected = isEditModeOn
        mapViewController.singleTapGesture.isEnabled = isEditModeOn
        mapViewController.longPressGesture.isEnabled = isEditModeOn
    }
    
    func showToolBox() {
        guard isToolBoxHidden else {
            return
        }
        
        isToolBoxHidden = false
        toolBoxWidthConstraint.constant = toolBoxMaxWidth
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func hideToolBox() {
        guard !isToolBoxHidden else {
            return
        }
        
        isToolBoxHidden = true
        toolBoxWidthConstraint.constant = toolBoxMinWidth
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func showToolTip() {
        guard isToolTipHidden else {
            return
        }
        
        isToolTipHidden = false
        toolTipContainerView.isHidden = false
        toolTipHeightConstraint.constant = toolTipMaxHeight
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func hideToolTip() {
        guard !isToolTipHidden else {
            return
        }
        
        isToolTipHidden = true
        toolTipHeightConstraint.constant = toolTipMinHeight
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.toolTipContainerView.isHidden = true
        })
    }
}

extension MainViewController: MapViewControllerDelegate {
    
    func routeDistanceUpdated(_ value: CLLocationDistance) {
        distanceLabel.text = "\(Int(value)/1852) nm"
    }
    
    func routeDistanceToolBoxShouldHide(_ aBool: Bool) {
        if aBool {
            hideToolBox()
        } else {
            showToolBox()
        }
    }
    
    func progressViewValueUpdatedTo(_ percent: Float) {
        progressView.setProgress(percent, animated: true)
    }
    
    func progressViewShouldHide(_ aBool: Bool) {
        progressView.isHidden = aBool
    }
}
