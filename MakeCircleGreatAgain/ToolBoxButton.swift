//
//  ToolBoxButton.swift
//  MakeCircleGreatAgain
//
//  Created by Andrea Ruffino on 04/03/2019.
//  Copyright © 2019 Andrea Ruffino. All rights reserved.
//

import UIKit

@IBDesignable class ToolBoxButton: UIButton {
    
    override var isSelected: Bool {
        get { return _isSelected_getter() }
        set { _isSelected_setter(newValue) }
    }
    
    override var isEnabled: Bool {
        get { return _isEnabled_getter() }
        set { _isEnabled_setter(newValue) }
    }
    
    override var isHighlighted: Bool {
        get { return _isHighlighted_getter() }
        set { _isHightlighted_setter(newValue) }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 5.0 {
        didSet { layer.cornerRadius = cornerRadius }
    }
    
    @IBInspectable var borderWidth: CGFloat = 1.0 {
        didSet { layer.borderWidth = borderWidth }
    }
    
    var borderEdgeInsets: UIEdgeInsets = UIEdgeInsets(top: 3.0, left: 3.0, bottom: 3.0, right: 3.0) {
        didSet { titleEdgeInsets = borderEdgeInsets }
    }
    
    // MARK: - Initialization and overridden methods
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func commonInit() {
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        titleEdgeInsets = borderEdgeInsets
        layer.borderColor = titleColor(for: self.state)?.cgColor
    }
    
    override func setTitleColor(_ color: UIColor?, for state: UIControl.State) {
        super.setTitleColor(color, for: state)
        layer.borderColor = titleColor(for: self.state)?.cgColor
    }
    
    // MARK: - Getters and setters
    
    private func _isSelected_getter() -> Bool {
        return super.isSelected
    }
    
    private func _isSelected_setter(_ newValue: Bool) {
        super.isSelected = newValue
        layer.borderColor = titleColor(for: self.state)?.cgColor
    }
    
    private func _isEnabled_getter() -> Bool {
        return super.isEnabled
    }
    
    private func _isEnabled_setter(_ newValue: Bool) {
        super.isEnabled = newValue
        layer.borderColor = titleColor(for: self.state)?.cgColor
    }
    
    private func _isHighlighted_getter() -> Bool {
        return super.isHighlighted
    }
    
    private func _isHightlighted_setter(_ newValue: Bool) {
        let kBorderColorKey = "borderColor"
        let highlightedColor = titleColor(for: self.state)?.withAlphaComponent(0.2).cgColor
        let normalColor = titleColor(for: self.state)?.cgColor
        self.layer.removeAnimation(forKey: kBorderColorKey)
        
        if (newValue) {
            layer.borderColor = highlightedColor
        } else {
            guard let fromColor = highlightedColor, let toColor = normalColor else {
                layer.borderColor = normalColor
                super.isHighlighted = newValue
                return
            }
            
            let animation = CABasicAnimation(keyPath: kBorderColorKey)
            animation.fromValue = fromColor
            animation.toValue = toColor
            animation.duration = 0.4
            self.layer.add(animation, forKey: kBorderColorKey)
            self.layer.borderColor = toColor
        }
        super.isHighlighted = newValue
    }
}
