//
//  File.swift
//  Lateral
//
//  Created by Andrea Ruffino on 05/12/2018.
//  Copyright © 2018 Andrea Ruffino. All rights reserved.
//

import UIKit
import Mapbox

// MARK: -

protocol MapViewControllerDelegate: AnyObject {
    
    func routeDistanceUpdated(_ value: CLLocationDistance)
    
    func progressViewValueUpdatedTo(_ percent: Float)
    
    func routeDistanceToolBoxShouldHide(_ aBool: Bool)
}

// MARK: -

class MapViewController: UIViewController {
    
    var mapView: MapView!
    
    var delegate: MapViewControllerDelegate?
    
    var route: RouteAnnotation!
    
    var trackRoute: TrackRouteAnnotation!
    
    var singleTapGesture: UITapGestureRecognizer!
    
    var longPressGesture: UILongPressGestureRecognizer!
    
    var selectionFeedbackGenerator: UISelectionFeedbackGenerator? = nil
    
    var impactFeedbackMediumGenerator: UIImpactFeedbackGenerator!
    
    var notificationFeedbackGenerator: UINotificationFeedbackGenerator!
    
    private var movingWaypoint: (RouteAnnotation, Int)? = nil
    
    private var longPressOffset: CGFloat = 0
    
    private let maximumLongPressOffset: CGFloat = 40
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Theme.current.name = .northStarLight
        
        notificationFeedbackGenerator = UINotificationFeedbackGenerator()
        impactFeedbackMediumGenerator = UIImpactFeedbackGenerator(style: .medium)
        
        mapView = MapView(frame: view.bounds, styleURL: Theme.current.styleUrl)
        mapView.delegate = self
        mapView.isPitchEnabled = false
        mapView.maximumZoomLevel = 14
        mapView.userTrackingMode = .followWithHeading
        mapView.showsUserHeadingIndicator = true
        view.addSubview(mapView)
        
        singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(handleCustomSingleTapGesture(_:)))
        mapView.addGestureRecognizer(singleTapGesture)
        
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleCustomLongPressGesture(_:)))
        mapView.addGestureRecognizer(longPressGesture)
        
        route = RouteAnnotation()
        trackRoute = TrackRouteAnnotation()
        trackRoute.lineWidth = 1
        trackRoute.lineColor = UIColor(value: 0x0ac49f)
        
        mapView.addAnnotation(self.route!)
        mapView.addAnnotation(self.trackRoute!)
    }
    
    @objc @IBAction func handleCustomSingleTapGesture(_ gesture: UITapGestureRecognizer) {
        let spot = gesture.location(in: mapView)
        let coordinate = mapView.convert(spot, toCoordinateFrom: mapView)
        
        if let (route, index) = findAnnotation(location: spot) {
            notificationFeedbackGenerator.notificationOccurred(.warning)
            impactFeedbackMediumGenerator.impactOccurred()
            route.removeWaypoint(at: index)
            route.update()
        } else {
            impactFeedbackMediumGenerator.impactOccurred()
            route.appendWaypoint(coordinate: coordinate)
            route.update()
        }
        updateTotalDistance()
    }
    
    @objc @IBAction func handleCustomLongPressGesture(_ gesture: UILongPressGestureRecognizer) {
        let spot = gesture.location(in: mapView)
        longPressOffset = (longPressOffset > 40) ? 40 : longPressOffset + 1
        let coordinate = mapView.convert(spot + CGPoint(x: 0, y: -longPressOffset), toCoordinateFrom: mapView)
        
        switch gesture.state {
        case .began:
            longPressOffset = 0
            if let (route, index) = findAnnotation(location: spot) {
                impactFeedbackMediumGenerator.impactOccurred()
                selectionFeedbackGenerator = UISelectionFeedbackGenerator()
                selectionFeedbackGenerator?.prepare()
                movingWaypoint = (route, index)
            }
            else {
                notificationFeedbackGenerator.notificationOccurred(.error)
                gesture.cancel()
            }
            
        case .changed:
            if let (route, index) = movingWaypoint {
                selectionFeedbackGenerator?.selectionChanged()
                selectionFeedbackGenerator?.prepare()
                route.removeWaypoint(at: index)
                route.insertWaypoint(coordinate: coordinate, at: index)
                route.update()
            }
            updateTotalDistance()
            
        case .cancelled, .ended, .failed:
            selectionFeedbackGenerator = nil
            movingWaypoint = nil
            
        default:
            break
        }
    }
    
    func findAnnotation(location: CGPoint) -> (RouteAnnotation, Int)? {
        for (index, pointAnnotation) in route.wPointAnnotations.enumerated() {
            guard let annotationView = pointAnnotation.annotationView else {
                continue
            }
            if annotationView.frame.insetBy(dx: -24, dy: -24).contains(location) {
                return (route, index)
            }
        }
        return nil
    }
    
    func updateTotalDistance() {
        delegate?.routeDistanceUpdated(route.totalDistance)
        if route.wPointAnnotations.isEmpty {
            delegate?.routeDistanceToolBoxShouldHide(true)
        } else {
            delegate?.routeDistanceToolBoxShouldHide(false)
        }
    }
}

// MARK: -

extension MapViewController:  MGLMapViewDelegate {
    
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        // PointAnnotation case
        if let waypointAnnotation = annotation as? PointAnnotation {
            
            // For better performance, always try to reuse existing annotations.
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: PointAnnotation.reuseIdentifier)
            
            // If there's no reusable annotation view available, initialize a new one.
            if annotationView == nil {
                annotationView = PointAnnotationView(reuseIdentifier: PointAnnotation.reuseIdentifier)
            }
            
            waypointAnnotation.annotationView = annotationView as? PointAnnotationView
            waypointAnnotation.updateAnnotationView()
            
            return annotationView /*as? WaypointAnnotationView*/
        }
        // MGLUserLocation case
        if annotation is MGLUserLocation && mapView.userLocation != nil {
            let userLocationAnnotationView = UserLocationAnnotationView()
            userLocationAnnotationView.delegate = self
            return userLocationAnnotationView
        }
        // Default case
        return nil
    }
    
    func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
        guard annotation is MGLUserLocation && mapView.userLocation != nil else {
            return
        }
        if mapView.userTrackingMode != .followWithHeading {
            mapView.userTrackingMode = .followWithHeading
        } else {
            mapView.resetNorth()
        }
        
        // We're borrowing this method as a gesture recognizer, so reset selection state.
        mapView.deselectAnnotation(annotation, animated: false)
    }
    
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        // Default case
        return true
    }
    
    func mapView(_ mapView: MGLMapView, alphaForShapeAnnotation annotation: MGLShape) -> CGFloat {
        if let annotation = annotation as? RoutePolyline {
            return annotation.alpha
        }
        // Fallback to the default alpha.
        return 1.0
    }
    
    func mapView(_ mapView: MGLMapView, lineWidthForPolylineAnnotation annotation: MGLPolyline) -> CGFloat {
        if let annotation = annotation as? RoutePolyline {
            return annotation.lineWidth
        }
        // Fallback to the default line width.
        return 1.0
    }
    
    func mapView(_ mapView: MGLMapView, strokeColorForShapeAnnotation annotation: MGLShape) -> UIColor {
        if let annotation = annotation as? RoutePolyline {
            return annotation.color
        }
        // Fallback to the default tint color.
        return mapView.tintColor
    }
}

extension MapViewController : MGLUserLocationAnnotationViewDelegate {
    
    func userLocationUpdated(to coordinate: CLLocationCoordinate2D, and direction: CLLocationDirection?) {
        if let direction = direction {
            trackRoute.computeTrackRoute(location: coordinate, direction: direction)
        } else {
            if trackRoute.initialDirection != nil {
                trackRoute.computeTrackRoute(location: coordinate, direction: trackRoute.initialDirection!)
            }
        }
    }
}
