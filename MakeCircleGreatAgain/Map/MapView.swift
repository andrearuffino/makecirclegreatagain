//
//  MapView.swift
//  Lateral
//
//  Created by Andrea Ruffino on 04/12/2018.
//  Copyright © 2018 Andrea Ruffino. All rights reserved.
//

import Mapbox

enum ZIndex: CGFloat {
    case highest =  2
    case high    =  1
    case normal  =  0
    case low     = -1
    case lowest  = -2
}

class MapView: MGLMapView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect, styleURL: URL?) {
        super.init(frame: frame, styleURL: styleURL)
        commonInit()
    }
    
    func commonInit() {
        self.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.showsScale = true
        self.logoView.isHidden = true
        // self.attributionButton.isHidden = true
    }
    
    override func updateConstraints() {
        super.updateConstraints()
    }
}
