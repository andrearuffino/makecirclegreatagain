//
//  UserLocationAnnotation.swift
//  MakeCircleGreatAgain
//
//  Created by Andrea Ruffino on 24/03/2019.
//  Copyright © 2019 Andrea Ruffino. All rights reserved.
//

import Foundation
import Mapbox

protocol MGLUserLocationAnnotationViewDelegate: AnyObject {
    
    func userLocationUpdated(to coordinate: CLLocationCoordinate2D, and direction: CLLocationDirection?)
}

// Create a subclass of MGLUserLocationAnnotationView.
class UserLocationAnnotationView: MGLUserLocationAnnotationView {
    
    let size: CGFloat = 18
    
    let color: UIColor = UIColor(value: 0x0ac49f)
    
    let borderWidth: CGFloat = 4
    
    var dot: CALayer!
    
    var arrow: CAShapeLayer!
    
    weak var delegate: MGLUserLocationAnnotationViewDelegate? = nil
    
    // -update is a method inherited from MGLUserLocationAnnotationView. It updates the appearance of the user location annotation when needed. This can be called many times a second, so be careful to keep it lightweight.
    override func update() {
        if frame.isNull {
            frame = CGRect(x: 0, y: 0, width: size, height: size)
            return setNeedsLayout()
        }
        
        // Check whether we have the user’s location yet.
        if CLLocationCoordinate2DIsValid(userLocation!.coordinate) /*&&
            !userLocation!.coordinate.latitude.isNaN &&
            !userLocation!.coordinate.longitude.isNaN*/ {
            setupLayers()
            updateHeading()
            
            delegate?.userLocationUpdated(to: userLocation!.coordinate, and: userLocation!.heading?.trueHeading)
        }
    }
    
    private func updateHeading() {
        // Show the heading arrow, if the heading of the user is available.
        if let heading = userLocation!.heading?.trueHeading {
            arrow.isHidden = false
            
            // Get the difference between the map’s current direction and the user’s heading, then convert it from degrees to radians.
            let rotation: CGFloat = -MGLRadiansFromDegrees(mapView!.direction - heading)
            
            // If the difference would be perceptible, rotate the arrow.
            if abs(rotation) > 0.01 {
                // Disable implicit animations of this rotation, which reduces lag between changes.
                CATransaction.begin()
                CATransaction.setDisableActions(true)
                arrow.setAffineTransform(CGAffineTransform.identity.rotated(by: rotation))
                CATransaction.commit()
            }
        } else {
            arrow.isHidden = true
        }
    }
    
    private func setupLayers() {
        // This dot forms the base of the annotation.
        if dot == nil {
            dot = CALayer()
            dot.bounds = CGRect(x: 0, y: 0, width: size, height: size)
            
            // Use CALayer’s corner radius to turn this layer into a circle.
            dot.cornerRadius = size / 2
            dot.backgroundColor = color.cgColor
            dot.borderWidth = borderWidth
            dot.borderColor = UIColor.white.cgColor
            layer.addSublayer(dot)
        }
        
        // This arrow overlays the dot and is rotated with the user’s heading.
        if arrow == nil {
            arrow = CAShapeLayer()
            arrow.path = arrowPath()
            arrow.frame = CGRect(x: 0, y: 0, width: size, height: size)
            arrow.position = CGPoint(x: dot.frame.midX, y: dot.frame.midY)
            arrow.fillColor = color.cgColor
            layer.addSublayer(arrow)
        }
    }
    
    // Calculate the vector path for an arrow, for use in a shape layer.
    private func arrowPath() -> CGPath {
        let max: CGFloat = size
        let mid: CGFloat = max * 0.5
        let width: CGFloat = 4

        let topLeft  = CGPoint(x: mid - (width/2), y: 0)
        let topRight = CGPoint(x: mid + (width/2), y: 0)
        let botLeft  = CGPoint(x: mid - (width/2), y: mid)
        let botRight = CGPoint(x: mid + (width/2), y: mid)
        
        let bezierPath = UIBezierPath()
        bezierPath.move(to: topLeft)
        bezierPath.addLine(to: topRight)
        bezierPath.addLine(to: botRight)
        bezierPath.addLine(to: botLeft)
        bezierPath.close()
        
        return bezierPath.cgPath
    }
}
