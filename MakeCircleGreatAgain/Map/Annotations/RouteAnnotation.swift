//
//  FlightPlanMapItem.swift
//  Guidor
//
//  Created by Skyconseil Dev on 28/05/2018.
//  Copyright © 2018 Skyconseil Dev. All rights reserved.
//

import Foundation
import Mapbox

// MARK: -

class RouteAnnotation: NSObject {
    
    static let DistanceForGreatCircle: CLLocationDistance = 10*1852 // nm
    
    static let DirectionForGreatCircle: CLLocationDistance = 1 // degree
    
    private(set) var fillPolyline: RoutePolyline? = nil
    
    private(set) var strokePolyline: RoutePolyline? = nil
    
    private(set) var wPointAnnotations: [PointAnnotation] = []
    
    private(set) var iPointAnnotations: [PointAnnotation] = []
    
    private(set) var wPointCoordinates: [CLLocationCoordinate2D] = []
    
    private(set) var iPointCoordinates: [CLLocationCoordinate2D] = []
    
    private var iPointCountPerWPoint = [Int]()
    
    var waypointCount: Int {
        return wPointCoordinates.count
    }
    
    var pointCount: Int {
        return iPointCoordinates.count
    }
    
    var totalDistance: CLLocationDistance {
        return RouteAnnotation.computeTotalDistance(waypoints: self.wPointAnnotations)
    }
    
    weak var mapView: MapView? {
        willSet { _mapViewWillSet(newValue) }
    }
    
    var lineColor: UIColor = Theme.current.routeLineColor {
        didSet { _lineColorDidSet() }
    }
    
    var lineStrokeColor: UIColor? = Theme.current.routeStrokeColor {
        didSet { _lineStrokeColorDidSet() }
    }
    
    var lineWidth: CGFloat = Theme.current.routeLineWidth {
        didSet { _lineWidthDidSet() }
    }
    
    var lineStrokeWidth: CGFloat = Theme.current.routeStrokeWidth {
        didSet { _lineStrokeWidthDidSet() }
    }
    
    var pointFillColor: UIColor = Theme.current.pointFillColor {
        didSet { _pointFillColorDidSet() }
    }
    
    var pointStrokeColor: UIColor? = Theme.current.pointStrokeColor {
        didSet { _pointStrokeColorDidSet() }
    }
    
    var pointSize: CGFloat = Theme.current.pointSize {
        didSet { _pointSizeDidSet() }
    }
    
    var pointStrokeWidth: CGFloat = Theme.current.pointStrokeWidth {
        didSet { _pointStrokeWidthDidSet() }
    }
    
    init(waypoints: [CLLocationCoordinate2D] = []) {
        super.init()
        
        wPointCoordinates = waypoints
        (iPointCoordinates, iPointCountPerWPoint) = RouteAnnotation.computeGreatCircleCoordinates(waypoints)
        
        updateFillPolyline()
        updateIPointAnnotations()
        updateWPointAnnotations()
    }
    
    func update() {
        updateStrokePolyline()
        updateFillPolyline()
        updateIPointAnnotations()
        updateWPointAnnotations()
    }
    
    private func updateFillPolyline() {
        if self.pointCount < 2 {
            if self.fillPolyline != nil {
                mapView?.removeAnnotation(self.fillPolyline!)
            }
            self.fillPolyline = nil
        } else {
            if self.fillPolyline == nil {
                self.fillPolyline = RoutePolyline(coordinates: self.iPointCoordinates, count: UInt(self.iPointCoordinates.count))
                self.fillPolyline!.color = lineColor
                self.fillPolyline!.lineWidth = lineWidth
                mapView?.addAnnotation(self.fillPolyline!)
            }
            else {
                self.fillPolyline!.setCoordinates(UnsafeMutablePointer(mutating: self.iPointCoordinates), count: UInt(self.iPointCoordinates.count))
            }
        }
    }
    
    private func updateStrokePolyline() {
        if self.pointCount < 2 || lineStrokeWidth <= 0 || lineStrokeColor == nil {
            if self.strokePolyline != nil {
                mapView?.removeAnnotation(self.strokePolyline!)
            }
            self.strokePolyline = nil
        } else {
            if self.strokePolyline == nil {
                self.strokePolyline = RoutePolyline(coordinates: self.iPointCoordinates, count: UInt(self.iPointCoordinates.count))
                self.strokePolyline!.color = lineStrokeColor!
                self.strokePolyline!.lineWidth = lineWidth + lineStrokeWidth
                mapView?.addAnnotation(self.strokePolyline!)
            }
            else {
                self.strokePolyline!.setCoordinates(UnsafeMutablePointer(mutating: self.iPointCoordinates), count: UInt(self.iPointCoordinates.count))
            }
        }
    }
    
    private func updateIPointAnnotations() {
        var cursor = iPointAnnotations.count
        while cursor > iPointCoordinates.count {
            if let annotation = iPointAnnotations.popLast() {
                mapView?.removeAnnotation(annotation)
            }
            cursor -= 1
        }
        for i in 0 ..< cursor {
            iPointAnnotations[i].coordinate = RouteAnnotation.checkCoordinate(iPointCoordinates[i])
        }
        while cursor < iPointCoordinates.count {
            let annotation = PointAnnotation(coordinate: RouteAnnotation.checkCoordinate(iPointCoordinates[cursor]))
            annotation.size = 2
            annotation.fillColor = UIColor.white
            annotation.strokeColor = nil
            annotation.strokeWidth = 0
            annotation.zPosition = .low
            iPointAnnotations.append(annotation)
            mapView?.addAnnotation(annotation)
            cursor += 1
        }
    }
    
    private func updateWPointAnnotations() {
        var cursor = wPointAnnotations.count
        while cursor > wPointCoordinates.count {
            if let annotation = wPointAnnotations.popLast() {
                mapView?.removeAnnotation(annotation)
            }
            cursor -= 1
        }
        for i in 0 ..< cursor {
            wPointAnnotations[i].coordinate = wPointCoordinates[i]
        }
        while cursor < wPointCoordinates.count {
            let annotation = PointAnnotation(coordinate: wPointCoordinates[cursor])
            annotation.size = self.pointSize
            annotation.fillColor = self.pointFillColor
            annotation.strokeColor = self.pointStrokeColor
            annotation.strokeWidth = self.pointStrokeWidth
            annotation.zPosition = .high
            wPointAnnotations.append(annotation)
            mapView?.addAnnotation(annotation)
            cursor += 1
        }
    }
    
    private func updateMapView(_ oldContext: MGLMapView?, _ newContext: MGLMapView?) {
        if oldContext != nil {
            if strokePolyline != nil { oldContext!.removeAnnotation(strokePolyline!) }
            if fillPolyline != nil { oldContext!.removeAnnotation(fillPolyline!) }
            oldContext!.removeAnnotations(wPointAnnotations)
            oldContext!.removeAnnotations(iPointAnnotations)
        }
        if newContext != nil {
            if strokePolyline != nil { newContext!.addAnnotation(strokePolyline!) }
            if fillPolyline != nil { newContext!.addAnnotation(fillPolyline!) }
            newContext!.addAnnotations(iPointAnnotations)
            newContext!.addAnnotations(wPointAnnotations)
        }
    }
    
    static func checkCoordinate(_ coordinate: CLLocationCoordinate2D) -> CLLocationCoordinate2D {
        var out = coordinate
        while out.longitude <= -180 { out.longitude += 360 }
        while out.longitude >   180 { out.longitude -= 360 }
        return out
    }
    
    func coordinateIndexForWaypoint(at wIndex: Int) -> Int {
        var index = 0
        for i in 0 ..< wIndex {
            index += iPointCountPerWPoint[i]
        }
        return index
    }
    
    func insertWaypoint(coordinate: CLLocationCoordinate2D, at index: Int) {
        self.insertWaypoint(coordinates: [coordinate], at: index)
    }
    
    func checkIPointCoordinatesCuttingAntiMeridian(after i_begin: Int = 1) {
        // Inputs longitude is between [-180:180], the 'mod' must be applied to get the shortest difference when crossing the antimeridian.
        let checkCuttingAntiMeridian: (inout CLLocationCoordinate2D, CLLocationCoordinate2D) -> () = {
            let delta1 = $0.longitude - $1.longitude
            let delta2 = $1.longitude - $0.longitude
            $0.longitude += (delta1 > 180) ? -360 : (delta2 > 180) ? 360 : 0
        }
        
        guard !iPointCoordinates.isEmpty else { return }
        let _i_begin = i_begin < 1 ? 1 : i_begin
        for i in _i_begin ..< iPointCoordinates.count {
            checkCuttingAntiMeridian(&iPointCoordinates[i],iPointCoordinates[i-1])
        }
    }
    
    func insertWaypoint(coordinates: [CLLocationCoordinate2D], at index: Int) {
        
        guard index >= 0, index <= wPointCoordinates.count else {
            return
        }
        
        let oldWaypointsCount = wPointCoordinates.count
        self.wPointCoordinates.insert(contentsOf: coordinates, at: index)
        
        guard wPointCoordinates.count > 2 else {
            // Waypoint count is always 1 or 2
            (iPointCoordinates, iPointCountPerWPoint) = RouteAnnotation.computeGreatCircleCoordinates(self.wPointCoordinates)
            return
        }
        
        var changedWaypoint = coordinates
        var w_index_min = index
        
        if index != 0 {
            w_index_min -= 1
            changedWaypoint.prepend(wPointCoordinates[w_index_min])
        }
        
        var _coordinates: [CLLocationCoordinate2D] = []
        var _indexes: [Int] = []
        let i_index_min = coordinateIndexForWaypoint(at: w_index_min)
        var i_index_max: Int!
        
        if index == oldWaypointsCount {
            (_coordinates, _indexes) = RouteAnnotation.computeGreatCircleCoordinates(changedWaypoint)
            i_index_max = coordinateIndexForWaypoint(at: index) + 1
        }
        else {
            changedWaypoint.append(wPointCoordinates[index + coordinates.count])
            (_coordinates, _indexes) = RouteAnnotation.computeGreatCircleCoordinates(changedWaypoint)
            let _ = _indexes.popLast()
            let _ = _coordinates.popLast()
            i_index_max = coordinateIndexForWaypoint(at: index)
        }
        
        let range  = i_index_min ..< i_index_max
        
        self.iPointCoordinates.removeSubrange(range)
        self.iPointCoordinates.insert(contentsOf: _coordinates, at: i_index_min)
        
        self.iPointCountPerWPoint.removeSubrange(w_index_min ..< index)
        self.iPointCountPerWPoint.insert(contentsOf: _indexes, at: w_index_min)
        
        self.checkIPointCoordinatesCuttingAntiMeridian(after: i_index_min)
    }
    
    func appendWaypoint(coordinate: CLLocationCoordinate2D) {
        self.insertWaypoint(coordinate: coordinate, at: self.wPointAnnotations.count)
    }

    func appendWaypoint(coordinates: [CLLocationCoordinate2D]) {
        self.insertWaypoint(coordinates: coordinates, at: self.wPointAnnotations.count)
    }
    
    func prependWaypoint(coordinate: CLLocationCoordinate2D) {
        self.insertWaypoint(coordinate: coordinate, at: 0)
    }

    func prependWaypoint(coordinates: [CLLocationCoordinate2D]) {
        self.insertWaypoint(coordinates: coordinates, at: 0)
    }
    
    func removeWaypointSubrange(_ range: ClosedRange<Int>) {
        guard !wPointAnnotations.isEmpty, range.lowerBound >= 0, range.upperBound < wPointAnnotations.count else {
            return
        }
        
        let oldWaypointsCount = wPointCoordinates.count
        wPointCoordinates.removeSubrange(range)
        
        guard wPointCoordinates.count > 2 else {
            // Waypoint count is always 0, 1 or 2
            if wPointCoordinates.isEmpty {
                self.iPointCountPerWPoint = []
                self.iPointCoordinates = []
            } else {
                (self.iPointCoordinates, self.iPointCountPerWPoint) = RouteAnnotation.computeGreatCircleCoordinates(self.wPointCoordinates)
            }
            return
        }
        
        var changedWaypoint: [CLLocationCoordinate2D] = []
        var w_index_min = range.lowerBound
        var w_index_max = range.upperBound
        
        if range.lowerBound != 0 {
            w_index_min -= 1
            changedWaypoint.append(wPointCoordinates[w_index_min])
        }
        
        if range.upperBound != oldWaypointsCount - 1 {
            changedWaypoint.append(wPointCoordinates[range.upperBound])
            w_index_max += 1
        }
        
        let i_index_min = coordinateIndexForWaypoint(at: w_index_min)
        let i_index_max = coordinateIndexForWaypoint(at: w_index_max)
        
        var _coordinates: [CLLocationCoordinate2D] = []
        var _indexes: [Int] = []
        
        (_coordinates, _indexes) = RouteAnnotation.computeGreatCircleCoordinates(changedWaypoint)
        if range.upperBound != oldWaypointsCount - 1 {
            let _ = _indexes.popLast()
        }
        
        self.iPointCoordinates.removeSubrange(i_index_min ... i_index_max)
        self.iPointCoordinates.insert(contentsOf: _coordinates, at: i_index_min)
        
        self.iPointCountPerWPoint.removeSubrange(w_index_min ... range.upperBound)
        self.iPointCountPerWPoint.insert(contentsOf: _indexes, at: w_index_min)
        
        self.checkIPointCoordinatesCuttingAntiMeridian(after: i_index_min)
    }
    
    func removeWaypointSubrange(_ range: Range<Int>) {
        guard !range.isEmpty else {
            return
        }
        return removeWaypointSubrange(ClosedRange<Int>(range))
    }
    
    func removeWaypoint(at index: Int) {
        return removeWaypointSubrange(index ... index)
    }
    
    func removeLastWaypoint() {
        return removeWaypoint(at: wPointAnnotations.count - 1)
    }
    
    func removeFirstWaypoint() {
        return removeWaypoint(at: 0)
    }
    
    func removeAllWaypoints() {
        self.mapView?.removeAnnotations(self.wPointAnnotations)
        self.wPointAnnotations.removeAll()
        self.wPointCoordinates = []
        
        self.mapView?.removeAnnotations(self.iPointAnnotations)
        self.iPointAnnotations.removeAll()
        self.iPointCoordinates = []
        
        self.iPointCountPerWPoint = []
    }
    
    static func computeTotalDistance(_ coordinates: [CLLocationCoordinate2D]) -> CLLocationDistance {
        var distance: CLLocationDistance = 0
        guard !coordinates.isEmpty else {
            return distance
        }
        for i in 0 ..< coordinates.count-1 {
            let coord_a = coordinates[i]
            let coord_b = coordinates[i+1]
            distance += coord_a.distance(from: coord_b)
        }
        return distance
    }
    
    static func computeTotalDistance(waypoints: [PointAnnotation]) -> CLLocationDistance {
        return computeTotalDistance(waypoints.map({ (annotation) -> CLLocationCoordinate2D in
            return annotation.coordinate
        }))
    }
    
    static func computeGreatCircleCoordinates(_ coordinates: [CLLocationCoordinate2D])  -> ([CLLocationCoordinate2D],[Int]) {
        
        // Inputs longitude is between [-180:180], the 'mod' must be applied to get the shortest difference when crossing the antimeridian.
        let checkCuttingAntiMeridian: (inout CLLocationCoordinate2D, CLLocationCoordinate2D) -> () = {
            let delta1 = $0.longitude - $1.longitude
            let delta2 = $1.longitude - $0.longitude
            $0.longitude += (delta1 > 180) ? -360 : (delta2 > 180) ? 360 : 0
        }
        
        var output = [CLLocationCoordinate2D]()
        var numberOfPointsAhead = 0
        var wIndexes = [Int]()
        guard !coordinates.isEmpty else {
            return (output, wIndexes)
        }
        
        // The loop must have a current and a next waypoint, thus one or less waypoint is impossible
        guard coordinates.count > 1 else {
            output.append(coordinates[0])
            wIndexes.append(numberOfPointsAhead)
            return (output, wIndexes)
        }
        
        for i in 0 ..< (coordinates.count - 1) {
            numberOfPointsAhead = 0
            var coord_a = coordinates[i]
            let coord_b = coordinates[i+1]
            let distance = DistanceForGreatCircle
            
            if (!output.isEmpty) {
                checkCuttingAntiMeridian(&coord_a, output.last!)
            }
            
            output.append(coord_a)
            numberOfPointsAhead += 1
            
            if coord_a.distance(from: coord_b) < 0.01 {
                wIndexes.append(numberOfPointsAhead)
                continue
            }
            var lastDirection = coord_a.azimuth(to: coord_b)
            var cursor = coord_a.atDistanceAndAzimuth(distance: distance, direction: lastDirection)
            
            //checkCuttingAntiMeridian(&cursor, coord_a)
            
            // For each segment, follow a direction given a track that is recomputed
            while cursor.distance(from: coord_b) > distance {
                let direction = cursor.azimuth(to: coord_b)
                if abs(direction - lastDirection) > DirectionForGreatCircle {
                    checkCuttingAntiMeridian(&cursor, output.last!)
                    output.append(cursor)
                    numberOfPointsAhead += 1
                    lastDirection = direction
                }
                
                //var nextCursor = cursor.atDistanceAndAzimuth(distance: distance, direction: cursor.azimuth(to: coord_b))
                //checkCuttingAntiMeridian(&nextCursor, cursor)
                cursor = cursor.atDistanceAndAzimuth(distance: distance, direction: cursor.azimuth(to: coord_b))
            }
            wIndexes.append(numberOfPointsAhead)
        }
        
        numberOfPointsAhead = 0
        // Output may be empty if input consists in waypoints having all the same coordinates, the last is added though.
        guard !output.isEmpty else {
            let index_last = coordinates.count - 1
            let coord_last = coordinates[index_last]
            
            output.append(coord_last)
            wIndexes.append(numberOfPointsAhead)
            
            return (output, wIndexes)
        }
        
        // Don't forget to append the last waypoint.
        let index_last = coordinates.count - 1
        var coord_last = coordinates[index_last]
        checkCuttingAntiMeridian(&coord_last, output.last!)
        
        output.append(coord_last)
        wIndexes.append(numberOfPointsAhead)
        return (output, wIndexes)
    }
    
    static func computeGreatCircleCoordinates(waypoints: [PointAnnotation]) -> ([CLLocationCoordinate2D],[Int]) {
        return computeGreatCircleCoordinates(waypoints.map({ (annotation) -> CLLocationCoordinate2D in
            return annotation.coordinate
        }))
    }
    
    // MARK: - Getters, setters, observables
    
    private func _mapViewWillSet(_ newValue: MapView?) {
        updateMapView(self.mapView, newValue)
    }
    
    private func _lineColorDidSet() {
        fillPolyline?.color = self.lineColor
    }
    
    private func _lineStrokeColorDidSet() {
        updateStrokePolyline()
        strokePolyline?.color = self.lineStrokeColor ?? self.lineColor
    }
    
    private func _lineWidthDidSet() {
        fillPolyline?.lineWidth = self.lineWidth
    }
    
    private func _lineStrokeWidthDidSet() {
        updateStrokePolyline()
        strokePolyline?.lineWidth = self.lineWidth + self.lineStrokeWidth
    }
    
    private func _pointFillColorDidSet() {
        for waypoint in wPointAnnotations {
            waypoint.fillColor = self.pointFillColor
        }
    }
    
    private func _pointStrokeColorDidSet() {
        for waypoint in wPointAnnotations {
            waypoint.strokeColor = self.pointStrokeColor
        }
    }
    
    private func _pointSizeDidSet() {
        for waypoint in wPointAnnotations {
            waypoint.size = self.pointSize
        }
    }
    
    private func _pointStrokeWidthDidSet() {
        for waypoint in wPointAnnotations {
            waypoint.strokeWidth = self.pointStrokeWidth
        }
    }
}

// MARK: -

class RoutePolyline: MGLPolyline {
    
    var color = Theme.current.routeLineColor
    
    var lineWidth = Theme.current.routeLineWidth
    
    var alpha: CGFloat = 1.0
    
    override init() {
        super.init()
    }
    
    convenience init(coordinates: UnsafePointer<CLLocationCoordinate2D>, count: UInt) {
        self.init()
        super.setCoordinates(UnsafeMutablePointer(mutating: coordinates), count: count)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

// MARK: -

extension MGLMapView {
    
    func addAnnotation(_ routeAnnotation: RouteAnnotation) {
        routeAnnotation.mapView = self as? MapView
    }
    
    func removeAnnotation(_ routeAnnotation: RouteAnnotation) {
        routeAnnotation.mapView = nil
    }
}
