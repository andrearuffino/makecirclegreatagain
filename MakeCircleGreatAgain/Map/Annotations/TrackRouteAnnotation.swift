//
//  TrackRouteAnnotation.swift
//  MakeCircleGreatAgain
//
//  Created by Andrea Ruffino on 01/06/2019.
//  Copyright © 2019 Andrea Ruffino. All rights reserved.
//

import Foundation
import Mapbox

class TrackRouteAnnotation: NSObject {

    static let DistanceForGreatCircle: CLLocationDistance = 10*1852 // nm
    
    static let DirectionForGreatCircle: CLLocationDistance = 1 // degree
    
    private(set) var fillPolyline: RoutePolyline? = nil
    
    private(set) var strokePolyline: RoutePolyline? = nil
    
    //private(set) var iPointAnnotations: [PointAnnotation] = []
    
    private var unsafeIPointCoordinates: [CLLocationCoordinate2D] = []
    
    var iPointCoordinates: [CLLocationCoordinate2D] {
        var copy: [CLLocationCoordinate2D]!
        concurrentObserverQueue.sync { copy = self.unsafeIPointCoordinates }
        return copy
    }
    
    private(set) var initialCoordinate: CLLocationCoordinate2D? = nil
    
    private(set) var initialDirection: CLLocationDirection? = nil
    
    var pointCount: Int {
        return iPointCoordinates.count
    }
    
    weak var mapView: MapView? {
        willSet { _mapViewWillSet(newValue) }
    }
    
    var lineColor: UIColor = Theme.current.routeLineColor {
        didSet { _lineColorDidSet() }
    }
    
    var lineStrokeColor: UIColor? = Theme.current.routeStrokeColor {
        didSet { _lineStrokeColorDidSet() }
    }
    
    var lineWidth: CGFloat = Theme.current.routeLineWidth {
        didSet { _lineWidthDidSet() }
    }
    
    var lineStrokeWidth: CGFloat = Theme.current.routeStrokeWidth {
        didSet { _lineStrokeWidthDidSet() }
    }
    
    private let concurrentObserverQueue =
        DispatchQueue(
            label: "com.andrearuffino.MakeCircleGreatAgain.TrackRouteAnnotation",
            attributes: .concurrent)
    
    init(location: CLLocationCoordinate2D? = nil, direction: CLLocationDirection? = nil) {
        super.init()
        
        guard location != nil && direction != nil else {
            return
        }
        
        computeTrackRoute(location: location!, direction: direction!)
    }
    
    func computeTrackRoute(location: CLLocationCoordinate2D, direction: CLLocationDirection) {
            
        concurrentObserverQueue.async(flags: .barrier) { [weak self] in
            guard let self = self else {
                return
            }
            
            if self.initialCoordinate != nil &&
                self.initialCoordinate!.distance(from: location) < 0.01 &&
                self.initialDirection != nil &&
                abs(self.initialDirection! - direction) < 0.5 {
                return
            }
            
            let checkCuttingAntiMeridian: (inout CLLocationCoordinate2D, CLLocationCoordinate2D) -> () = {
                let delta1 = $0.longitude - $1.longitude
                let delta2 = $1.longitude - $0.longitude
                $0.longitude += (delta1 > 180) ? -360 : (delta2 > 180) ? 360 : 0
            }
            
            self.initialCoordinate = location
            self.initialDirection = direction
            self.unsafeIPointCoordinates.removeAll()
            self.unsafeIPointCoordinates.append(self.initialCoordinate!)
            
            let antipod_lat = -self.initialCoordinate!.latitude
            var antipod_lon = self.initialCoordinate!.longitude - 180
            antipod_lon += (antipod_lon < -180) ? 360 : 0
            
            var coord_a = self.initialCoordinate!
            var coord_b = CLLocationCoordinate2D(latitude: antipod_lat, longitude: antipod_lon)
            let distance = TrackRouteAnnotation.DistanceForGreatCircle
            
            var lastTrack = self.initialDirection!
            var cursor = coord_a.atDistanceAndAzimuth(distance: distance, direction: self.initialDirection!)
            
            while cursor.distance(from: coord_b) > distance {
                let track = cursor.azimuth(to: coord_b)
                if abs(track - lastTrack) > TrackRouteAnnotation.DirectionForGreatCircle {
                    checkCuttingAntiMeridian(&cursor, self.unsafeIPointCoordinates.last!)
                    self.unsafeIPointCoordinates.append(cursor)
                    lastTrack = track
                }
                
                cursor = cursor.atDistanceAndAzimuth(distance: distance, direction: cursor.azimuth(to: coord_b))
            }
            
            checkCuttingAntiMeridian(&coord_b, self.unsafeIPointCoordinates.last!)
            self.unsafeIPointCoordinates.append(coord_b)
            cursor = coord_b.atDistanceAndAzimuth(distance: distance, direction: lastTrack)
            
            while cursor.distance(from: coord_a) > distance {
                let track = cursor.azimuth(to: coord_a)
                if abs(track - lastTrack) > TrackRouteAnnotation.DirectionForGreatCircle {
                    checkCuttingAntiMeridian(&cursor, self.unsafeIPointCoordinates.last!)
                    self.unsafeIPointCoordinates.append(cursor)
                    lastTrack = track
                }
                
                cursor = cursor.atDistanceAndAzimuth(distance: distance, direction: cursor.azimuth(to: coord_a))
            }
            
            checkCuttingAntiMeridian(&coord_a, self.unsafeIPointCoordinates.last!)
            self.unsafeIPointCoordinates.append(coord_a)
            
            DispatchQueue.main.async { [weak self] in
                self?.update()
            }
        }
    }
    
    func update() {
        updateStrokePolyline()
        updateFillPolyline()
        //updateIPointAnnotations()
    }
    
    private func updateFillPolyline() {
        if self.pointCount < 2 {
            if self.fillPolyline != nil {
                mapView?.removeAnnotation(self.fillPolyline!)
            }
            self.fillPolyline = nil
        } else {
            if self.fillPolyline == nil {
                self.fillPolyline = RoutePolyline(coordinates: self.iPointCoordinates, count: UInt(self.iPointCoordinates.count))
                self.fillPolyline!.color = lineColor
                self.fillPolyline!.lineWidth = lineWidth
                mapView?.addAnnotation(self.fillPolyline!)
            }
            else {
                self.fillPolyline!.setCoordinates(UnsafeMutablePointer(mutating: self.iPointCoordinates), count: UInt(self.iPointCoordinates.count))
            }
        }
    }
    
    private func updateStrokePolyline() {
        if self.pointCount < 2 || lineStrokeWidth <= 0 || lineStrokeColor == nil {
            if self.strokePolyline != nil {
                mapView?.removeAnnotation(self.strokePolyline!)
            }
            self.strokePolyline = nil
        } else {
            if self.strokePolyline == nil {
                self.strokePolyline = RoutePolyline(coordinates: self.iPointCoordinates, count: UInt(self.iPointCoordinates.count))
                self.strokePolyline!.color = lineStrokeColor!
                self.strokePolyline!.lineWidth = lineWidth + lineStrokeWidth
                mapView?.addAnnotation(self.strokePolyline!)
            }
            else {
                self.strokePolyline!.setCoordinates(UnsafeMutablePointer(mutating: self.iPointCoordinates), count: UInt(self.iPointCoordinates.count))
            }
        }
    }
    
    private func updateMapView(_ oldContext: MGLMapView?, _ newContext: MGLMapView?) {
        if oldContext != nil {
            if strokePolyline != nil { oldContext!.removeAnnotation(strokePolyline!) }
            if fillPolyline != nil { oldContext!.removeAnnotation(fillPolyline!) }
            //oldContext!.removeAnnotations(iPointAnnotations)
        }
        if newContext != nil {
            if strokePolyline != nil { newContext!.addAnnotation(strokePolyline!) }
            if fillPolyline != nil { newContext!.addAnnotation(fillPolyline!) }
            //newContext!.addAnnotations(iPointAnnotations)
        }
    }
    
    // MARK: - Getters, setters, observables
    
    private func _mapViewWillSet(_ newValue: MapView?) {
        updateMapView(self.mapView, newValue)
    }
    
    private func _lineColorDidSet() {
        fillPolyline?.color = self.lineColor
    }
    
    private func _lineStrokeColorDidSet() {
        updateStrokePolyline()
        strokePolyline?.color = self.lineStrokeColor ?? self.lineColor
    }
    
    private func _lineWidthDidSet() {
        fillPolyline?.lineWidth = self.lineWidth
    }
    
    private func _lineStrokeWidthDidSet() {
        updateStrokePolyline()
        strokePolyline?.lineWidth = self.lineWidth + self.lineStrokeWidth
    }
}

// MARK: -

extension MGLMapView {
    
    func addAnnotation(_ routeAnnotation: TrackRouteAnnotation) {
        routeAnnotation.mapView = self as? MapView
    }
    
    func removeAnnotation(_ routeAnnotation: TrackRouteAnnotation) {
        routeAnnotation.mapView = nil
    }
}
