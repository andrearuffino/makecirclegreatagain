//
//  WaypointAnnotation.swift
//  Guidor
//
//  Created by Andrea Ruffino on 02/05/2018.
//  Copyright © 2018 Skyconseil Dev. All rights reserved.
//

import Foundation
import Mapbox

// MARK: -

class PointAnnotation: MGLPointAnnotation {
    
    static let reuseIdentifier: String = "PointAnnotation"
    
    weak var annotationView: PointAnnotationView?
    
    var zPosition: ZIndex = .normal
    
    var fillColor: UIColor = Theme.current.pointFillColor {
        didSet {
            annotationView?.fillColor = self.fillColor
        }
    }
    
    var strokeColor: UIColor? = Theme.current.pointStrokeColor {
        didSet {
            annotationView?.strokeColor = self.strokeColor
        }
    }
    
    var size: CGFloat = Theme.current.pointSize {
        didSet {
            annotationView?.size = self.size
        }
    }
    
    var strokeWidth: CGFloat = Theme.current.pointStrokeWidth {
        didSet {
            annotationView?.strokeWidth = self.strokeWidth
        }
    }
    
    var isHidden: Bool = false {
        didSet {
            annotationView?.isHidden = self.isHidden
        }
    }
    
    init(coordinate: CLLocationCoordinate2D) {
        super.init()
        self.coordinate = coordinate
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func updateAnnotationView() {
        guard let annotationView = annotationView else {
            return
        }
        annotationView.fillColor = self.fillColor
        annotationView.size = self.size
        annotationView.strokeColor = self.strokeColor
        annotationView.strokeWidth = self.strokeWidth
        annotationView.isHidden = self.isHidden
        annotationView.layer.zPosition = self.zPosition.rawValue
    }
}

// MARK: -

class PointAnnotationView : MGLAnnotationView {
    
    fileprivate var fillColor : UIColor = Theme.current.pointFillColor {
        didSet {
            self.backgroundColor = self.fillColor
        }
    }
    
    fileprivate var strokeColor: UIColor? = nil {
        didSet {
            self.layoutSubviews()
        }
    }
    
    fileprivate var size : CGFloat = Theme.current.pointSize {
        didSet {
            let totalSize = self.size + self.strokeWidth
            self.frame = CGRect(center: self.frame.center, size: CGSize(width: totalSize, height: totalSize))
        }
    }
    
    fileprivate var strokeWidth: CGFloat = Theme.current.pointStrokeWidth {
        didSet {
            self.layoutSubviews()
        }
    }
    
    override init(reuseIdentifier: String? = nil) {
        super.init(reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        let totalSize = size + strokeWidth
        self.frame = CGRect(x: 0, y: 0, width: totalSize, height: totalSize)
        self.backgroundColor = fillColor
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Force the annotation view to maintain a constant size when the map is tilted.
        scalesWithViewingDistance = false
        
        layer.cornerRadius = frame.width / 2
        layer.borderColor = strokeColor?.cgColor
        layer.borderWidth = (strokeColor != nil) ? strokeWidth : 0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        let totalSize = size + strokeWidth
        let cgSize = CGSize(width: totalSize, height: totalSize)
        let toFrame = CGRect(center: self.frame.center, size: selected ? cgSize*2 : cgSize)
        
        if animated {
            UIView.animate(withDuration: 0.2) {
                self.frame = toFrame
                self.layoutSubviews()
            }
        } else {
            self.frame = toFrame
            self.layoutSubviews()
        }
    }
}
