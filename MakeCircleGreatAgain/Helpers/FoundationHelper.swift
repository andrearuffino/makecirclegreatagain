//
//  ImageUtils.swift
//  Lateral
//
//  Created by Andrea Ruffino on 05/12/2018.
//  Copyright © 2018 Andrea Ruffino. All rights reserved.
//

import Foundation
import UIKit

// class WeakRef<T> where T: AnyObject {
//
//     private(set) weak var value: T?
//     init(value: T?) {
//         self.value = value
//     }
// }

// MARK: -

extension UIGestureRecognizer {
    
    func cancel() {
        isEnabled = false
        isEnabled = true
    }
}

// MARK: -

extension Array {
    
    mutating func prepend(_ newElement: __owned Element) {
        self.insert(newElement, at: 0)
    }
    
    mutating func prepend<S>(contentsOf newElements: __owned S) where S : Sequence, Element == S.Element {
        self.insert(contentsOf: Array(newElements), at: 0)
    }
}

// MARK: -

extension CGVector {
    
    init(_ p: CGPoint) {
        self.init(dx: p.x, dy: p.y)
    }
    
    var length: CGFloat {
        return sqrt((dx * dx) + (dy * dy))
    }
}

// MARK: -

extension CGPoint {
    
    init(_ v: CGVector) {
        self.init(x: v.dx, y: v.dy)
    }
}

// MARK: -

extension CGRect {
    
    var center: CGPoint {
        get {
            return CGPoint(x: self.midX, y: self.midY)
        }
        set {
            self.origin.x = newValue.x - (width / 2)
            self.origin.y = newValue.y - (height / 2)
        }
    }
    
    init(center: CGPoint, size: CGSize) {
        self.init(origin: .zero, size: size)
        self.center = center
    }
}

// MARK: - CGPoint operator overloading

func * (left: CGPoint, right: CGFloat) -> CGPoint {
    return CGPoint(x: left.x * right, y: left.y * right)
}

func * (left: CGFloat, right: CGPoint) -> CGPoint {
    return right * left
}

func / (left: CGPoint, right: CGFloat) -> CGPoint {
    return CGPoint(x: left.x / right, y: left.y / right)
}

func + (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

func - (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x - right.x, y: left.y - right.y)
}

func * (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x * right.x, y: left.y * right.y)
}

func / (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x / right.x, y: left.y / right.y)
}

func += (left: inout CGPoint, right: CGPoint) {
    left.x = left.x + right.x
    left.y = left.y + right.y
}

func -= (left: inout CGPoint, right: CGPoint) {
    left.x = left.x - right.x
    left.y = left.y - right.y
}

func *= (left: inout CGPoint, right: CGFloat) {
    left = left * right
}

func /= (left: inout CGPoint, right: CGFloat) {
    left = left / right
}

prefix func - (point: CGPoint) -> CGPoint {
    return CGPoint(x: -point.x, y: -point.y)
}

// MARK: - CGSize operator overloading

func * (left: CGSize, right: CGFloat) -> CGSize {
    return CGSize(width: left.width * right, height: left.height * right)
}

func * (left: CGFloat, right: CGSize) -> CGSize {
    return right * left
}

func / (left: CGSize, right: CGFloat) -> CGSize {
    return CGSize(width: left.width / right, height: left.height / right)
}

func *= (left: inout CGSize, right: CGFloat) {
    left = left * right
}

func /= (left: inout CGSize, right: CGFloat) {
    left = left / right
}
