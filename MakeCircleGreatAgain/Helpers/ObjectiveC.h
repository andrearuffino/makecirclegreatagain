//
//  NSObject+ObjectiveC.h
//  MakeCircleGreatAgain
//
//  Created by Andrea Ruffino on 10/06/2019.
//  Copyright © 2019 Andrea Ruffino. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjectiveC : NSObject

+ (bool) catchException:(void(^)())tryBlock error:(__autoreleasing NSError **)error;

@end

