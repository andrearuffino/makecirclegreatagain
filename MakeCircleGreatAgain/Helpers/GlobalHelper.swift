//
//  AssetUtils.swift
//  Lateral
//
//  Created by Andrea Ruffino on 05/12/2018.
//  Copyright © 2018 Andrea Ruffino. All rights reserved.
//

import UIKit
import CoreLocation

// MARK: -

class Theme {
    
    enum Name { case northStarEarth, northStarLight, basicTemplate }
    
    private(set) static var current = Theme(named: .northStarEarth)
    
    var name: Name {
        didSet {
            updateTheme(to: self.name)
        }
    }
    
    private(set) var styleUrl: URL
    
    private(set) var pointFillColor: UIColor
    
    private(set) var pointStrokeColor: UIColor?
    
    private(set) var pointSize: CGFloat
    
    private(set) var pointStrokeWidth: CGFloat
    
    private(set) var routeLineColor: UIColor
    
    private(set) var routeStrokeColor: UIColor?
    
    private(set) var routeLineWidth: CGFloat
    
    private(set) var routeStrokeWidth: CGFloat
    
    private init(named themeName: Name) {
        self.name = themeName
        
        switch themeName {
            
        case .northStarLight:
            styleUrl = URL(string: "mapbox://styles/andrearuffino/cjt8vhshv1lam1fqrqo6oac0u")!
            
            pointFillColor = .darkGray
            pointStrokeColor = .orange
            pointSize = 6
            pointStrokeWidth = 2
            
            routeLineColor = .orange
            routeLineWidth = 2
            routeStrokeWidth = 0
            
        case .northStarEarth:
            styleUrl = URL(string: "mapbox://styles/andrearuffino/cjss89c841bz41gs05v6se0ej")!
            
            pointFillColor = .darkGray
            pointStrokeColor = .orange
            pointSize = 6
            pointStrokeWidth = 2
            
            routeLineColor = .orange
            routeLineWidth = 2
            routeStrokeWidth = 0
            
        case .basicTemplate:
            styleUrl = URL(string: "mapbox://styles/andrearuffino/cjsu7r7jo1zbj1ft7b1a8quqz")!
            
            pointFillColor = .white
            pointSize = 6
            pointStrokeWidth = 0
            
            routeLineColor = .orange
            routeLineWidth = 2
            routeStrokeWidth = 0
        }
    }
    
    private func updateTheme(to themeName: Name) {
        Theme.current = Theme(named: themeName)
    }
}

// MARK: -

extension CLLocationCoordinate2D {
    
    static let paris = CLLocationCoordinate2D(latitude: 48.8587598, longitude: 2.3526083)
    
    static let toulouse = CLLocationCoordinate2D(latitude: 43.5477593, longitude: 1.4561742)
    
    static let grenoble = CLLocationCoordinate2D(latitude: 45.1799004, longitude: 5.7280013)
    
    static let marseille = CLLocationCoordinate2D(latitude: 43.2938923, longitude: 5.3822524)
    
    static let brest = CLLocationCoordinate2D(latitude: 48.3972074, longitude: -4.4734944)
}
