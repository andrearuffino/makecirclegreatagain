//
//  NSObject+ObjectiveC.m
//  MakeCircleGreatAgain
//
//  Created by Andrea Ruffino on 10/06/2019.
//  Copyright © 2019 Andrea Ruffino. All rights reserved.
//

#import "ObjectiveC.h"

@implementation ObjectiveC

+ (bool) catchException:(void(^)())tryBlock error:(__autoreleasing NSError **)error {
    @try {
        tryBlock();
        return true;
    }
    @catch (NSException *exception) {
        *error = [[NSError alloc] initWithDomain:exception.name code:0 userInfo:exception.userInfo];
        return false;
    }
}

@end
