//
//  Utils.swift
//  Lateral
//
//  Created by Andrea Ruffino on 04/12/2018.
//  Copyright © 2018 Andrea Ruffino. All rights reserved.
//

import CoreLocation

// MARK: -

extension CLLocation {
    
    static let EarthMeanRadius = 6371.0072
    
    static func distance(from coord1: CLLocationCoordinate2D, to coord2: CLLocationCoordinate2D) -> CLLocationDistance {
        return coord2.distance(from: coord1)
    }
    
    static func nauticalMilesDistance(from coord1: CLLocationCoordinate2D, to coord2: CLLocationCoordinate2D) -> Double {
        return distance(from: coord1, to: coord2) / 1852
    }
}

// MARK: -

extension CLLocationCoordinate2D {
    
    func distance(from: CLLocationCoordinate2D) -> CLLocationDistance {
        let coord1 = CLLocation(latitude: self.latitude, longitude: self.longitude)
        let coord2 = CLLocation(latitude: from.latitude, longitude: from.longitude)
        return coord1.distance(from: coord2)
    }
    
    func azimuth(to: CLLocationCoordinate2D) -> CLLocationDirection {
        let degreesToRadians: (Double) -> Double = { return $0 / 180.0 * Double.pi }
        let radiansToDegrees: (Double) -> Double = { return $0 * 180.0 / Double.pi }
        
        let dLon_rad = degreesToRadians(to.longitude - self.longitude)
        let lat1_rad = degreesToRadians(self.latitude)
        let lat2_rad = degreesToRadians(to.latitude)
        
        let y = sin(dLon_rad) * cos(lat2_rad)
        let x = cos(lat1_rad) * sin(lat2_rad) - sin(lat1_rad) * cos(lat2_rad) * cos(dLon_rad)
        
        let azimuth = radiansToDegrees(atan2(y, x)) + 360.0
        var whole = Double.nan
        let fraction = modf(azimuth, &whole)
        return CLLocationDirection(Double(Int(whole + 360) % 360) + fraction)
    }
    
    private static func atDistanceAndAzimuth(from: CLLocationCoordinate2D, distance: CLLocationDistance, direction: CLLocationDirection, _ lon_deg: inout CLLocationDegrees, _ lat_deg: inout CLLocationDegrees) {
        let degreesToRadians: (Double) -> Double = { return $0 / 180.0 * Double.pi }
        let radiansToDegrees: (Double) -> Double = { return $0 * 180.0 / Double.pi }
        
        let lat_rad = degreesToRadians(from.latitude)
        let lon_rad = degreesToRadians(from.longitude)
        let cosLat_rad = cos(lat_rad)
        let sinLat_rad = sin(lat_rad)
        
        let azimuthRad = degreesToRadians(direction)
        
        let ratio = (distance / (CLLocation.EarthMeanRadius * 1000.0))
        let cosRatio = cos(ratio)
        let sinRatio = sin(ratio)
        
        let _lat_rad = asin(sinLat_rad * cosRatio + cosLat_rad * sinRatio * cos(azimuthRad))
        let _lon_rad = lon_rad + atan2(sin(azimuthRad) * sinRatio * cosLat_rad, cosRatio - sinLat_rad * sin(_lat_rad))
        
        lat_deg = radiansToDegrees(_lat_rad)
        lon_deg = radiansToDegrees(_lon_rad)
    }
    
    func atDistanceAndAzimuth(distance: CLLocationDistance, direction: CLLocationDirection) -> CLLocationCoordinate2D {
        var lon_deg: CLLocationDegrees = Double.nan
        var lat_deg: CLLocationDegrees = Double.nan
        CLLocationCoordinate2D.atDistanceAndAzimuth(from: self, distance: distance, direction: direction, &lon_deg, &lat_deg)

        return CLLocationCoordinate2DMake(lat_deg, lon_deg)
    }
}
